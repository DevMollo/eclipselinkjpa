/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Entidades.Categorias;
import Modelo.Persistencia.CategoriasJpaController;
import Vista.VistaCategorias;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author MOLLODEB
 */
public class CategoriaController implements ActionListener{
    private VistaCategorias vista;
    private CategoriasJpaController catJpa;

    public CategoriaController() {
        catJpa =new CategoriasJpaController();
        vista=new VistaCategorias();
        Inicializar();
    }

    public CategoriaController(VistaCategorias vista, CategoriasJpaController catJpa) {
        this.vista = vista;
        this.catJpa = catJpa;
    }
    
    private void Inicializar(){
        this.vista.setVisible(true);
        this.vista.getBtnGuardar().addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource().equals(this.vista.getBtnGuardar())){
            Categorias cat=new Categorias();
            cat.setNombre(vista.getTxtNombre().getText());
            catJpa.create(cat);
        }
    }
    
    
}
